#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<string.h>
#include<fcntl.h>
#include"explorer.h"
#include"premier.h"
#include"my_system.h"

void explorer(int debut, int fin)
  {
     int etat,pid,pid2;
     pid=fork();
     if(pid==0)
       {
           for (int i=debut; i<=fin;i++)
            {
              if (premier(i)==1)
              {
                 pid2=fork();
                fflush(stdout);
                 if (pid2==0)
                   {
                       char chaine[100];
                       sprintf(chaine,"%d est un nombre premier, écrit par le processus %d dont le père est le processus %d",i, getpid(), getppid());
                       my_system ("/bin/echo", chaine);
                       sleep(2);
                       exit(0);
                   }
                else waitpid(pid2, &etat, WNOHANG);
              }

            }
              sleep(1);
              exit(0);
          }
      else waitpid(pid, &etat, WNOHANG);
  }
