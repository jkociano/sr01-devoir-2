#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<string.h>
#include<fcntl.h>

void my_system (char *path, char *args)
{
  char *commande[] = {"echo", args, NULL};
  execv (path, commande);
}

int premier(int nb)
  {
    int r=0;
    for(int i = 1 ; i <= nb ; i++ )
      {
          if(nb % i == 0) r++;
      }
    if(r>2)
       return 0;
    else
       return 1;
  }


void explorer(int debut, int fin)
  {
     int etat,pid,pid2;
     pid=fork();
     if(pid==0)
       {
           for (int i=debut; i<=fin;i++)
            {
              if (premier(i)==1)
              {
                 pid2=fork();
                fflush(stdout);
                 if (pid2==0)
                   {
                       char chaine[100];
                       sprintf(chaine,"%d est un nombre premier, écrit par le processus %d dont le père est le processus %d",i, getpid(), getppid());
                       my_system ("/bin/echo", chaine);
                       sleep(2);
                       exit(0);
                   }
                else

                  wait(&etat); // instruction 41
              }

            }
              exit(0);
          }
      else wait(&etat);
  }

int main()
  {
      int grp=1;
      close (1);
      open("nbr_premiers_2.txt", O_RDWR | O_CREAT | O_TRUNC |O_APPEND, 0666);

      while(grp<=11)
        {
            explorer(grp+1,grp+10);
            grp=grp+10;
        }
  }
