#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#define TAILLE 1000


int main(int argc, char const *argv[]) {
  char chaine[TAILLE] = "";
  FILE* fichier = NULL;
  fichier = fopen("list_appli.txt", "r");
  if (fichier != NULL)
  {
    fgets (chaine, TAILLE, fichier);
    char *key = strtok(chaine, "=");
    char *value = strtok(NULL, "=");
    int nbr = atoi(value);
    if (strcmp (key, "nombre_applications") == 0)
    {
      printf ("nous avons %d applications \n", nbr);
    }
    while (fgets (chaine, TAILLE, fichier) != NULL)
    {
      char *cle = strtok(chaine, "=");
      char *valeur = strtok(NULL, "=");
        if (strcmp(cle, "name") == 0)
        {
          char *nom = valeur;
          printf("L'application est %s,", nom);
        }
        else if (strcmp (cle, "path") == 0)
        {
          char *path = valeur;
          printf("son chemin relatif est : %s.", path);
        }
        else if (strcmp (cle, "nombre_arguments") == 0)
        {
          int args = atoi(valeur);
          printf("Elle a %d arguments\n\n", args);
          if (args != 0)
          {
            fgets (chaine, TAILLE, fichier);
            for (int i = 0; i < args; i++)
            {
              fgets (chaine, TAILLE, fichier);
              printf("Argument : %s\n", chaine);
            }
          }
        }

    }




  fclose(fichier);
  }
  else
  perror ("erreur d'ouverture");
  return 0;
}
