#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
//#include <ApplicationManager.h>
#define LINESIZE 1000

typedef struct Appli{
    int id;
    char* name;
    char* path;
    int nbArg;
    char** args;
}Appli;

void signal_handler (int signo){
/* On crée une fonction signal_handler pour traiter
la réception du signal émis par power_manager.c */
    
    printf("Le signal a bien été reçu, nous fermons les applications");
    fflush(stdout);
    kill(0, SIGTERM);
  // On ferme tous les processus du groupe via le signal SIGTERM
    }

    
char* removeCRLF (char * s){
/* Cette fonction nous permet de retirer tous les 
retour à la ligne d'une chaine de caractères */
    
    char *tmp = strchr(s ,'\r');
    if (tmp != NULL)
        *tmp = 0;

    tmp = strchr(s ,'\n');
    if (tmp != NULL)
        *tmp = 0;
    return s;
    }
    
char* parseValue (char* s){
/* Cette fonction nous permet de récuperer la valeur
contenu après un signe "=" dans un fichier */

  char *key = strtok(s, "=");
  // strtok permet de diviser une chaine en 2 en fonction d'un séparateur
  char *value = strtok(NULL, "=");
  printf("valeur recup value: %s \n",value);
  //On utilise l'argument NULL pour récuperer la 2eme partie de la chaine
  char* result = malloc((strlen(value)+1) * sizeof(char));
  /* On crée une variable result qui permettra d'accueillir la chaine
  On utilise une allocation dynamique car on ne connait pas la taille de la
  chaine */
  strcpy(result, value);
  // On copie la chaine dans result
  removeCRLF(result);
  printf("valeur recup result: %s \n", result);
  //On supprime les retours à la ligne
  return result;
}

int parseNumber (char * s){
/* Cette fonction nous permet de récuperer 
la valeur d'un entier contenu après un "=" */

  int number;
  char* valeur = parseValue(s);
  if (valeur == NULL)
    {
      perror("nous n'avons pas récupéré la chaine");
      exit(EXIT_FAILURE);
      //On vérifie qu'on a bien récupéré la chaine
    }
  number = atoi(valeur);
  //On utilise atoi pour convertir un ASCII en int
  free(valeur);
  // On libère la mémoire allouée
  return number;
}

int main(int argc, char const *argv[]){
    char line[LINESIZE];
    char* nom = "";
    char* pat = "";
    char* lecture = "";
    char* arg = "";
    int nbr_app, nbrArg, nbProcessus = 0;
    int pid;
    Appli* applis = NULL;
    FILE* fichier = fopen("list_appli.txt", "r");
/* On initialise une variable line ainsi qu'une variable nbr_app 
pour nos futurs appels de fonction. On ouvre le fichier 
"list_appli.txt en lecture*/ 
    if (fichier == NULL){
        perror ("Can't read list_appli.txt file");
        return 1;
    /* Message d'erreur si le fichier ne s'ouvre pas*/
    }
    fgets(line, LINESIZE, fichier);
    nbr_app = parseNumber(line);
    if (nbr_app == 0){
      perror("Soit il n'y a pas d'application, soit nous n'avons pas réussi à récupérer leur nombre");
      exit(EXIT_FAILURE);
      //On vérifie qu'on a bien récupéré le nombre d'applications
    }
    printf("Le fichier list_appli.txt contient %d applications.\n", nbr_app);
    applis = malloc(nbr_app*sizeof(Appli));
    if(applis == NULL){
        perror("aucune mémoire assigné au tableau dynamique des application");
        exit(EXIT_FAILURE);
    }
    else{
        printf("malloc applis ok\n");
    }
    
    
    //ici on va parcourir le txt pour récup les infos qui nous intéressent*/
    
    for (int i = 0; i < nbr_app; i++){
        //fgets(line, LINESIZE, fichier);
        if (fgets(line, LINESIZE, fichier) == NULL){
            perror("nous n'avons pas récupéré la chaine");
            exit(EXIT_FAILURE);
            //On vérifie que fgets a bine fonctionné
        }
        nom = parseValue(line); //récuper le nom de l'appli
        if (nom == NULL)
        {
            perror("nous n'avons pas récupéré le nom");
            exit(EXIT_FAILURE);
            //On vérifie qu'on a bien récupéré le nom
        }
        printf("name ok\n");
        applis[i].name = nom;
        printf("nom ok: i = %d : nom = %s\n", i,applis[i].name );
        
        lecture = fgets(line, LINESIZE, fichier);
        if (lecture == NULL){
            perror("nous n'avons pas récupéré la chaine");
            exit(EXIT_FAILURE);
        //On vérifie que fgets a bien fonctionné
        }
        
        pat = parseValue(line);
        if (pat == NULL)
        {
            perror("nous n'avons pas récupéré le nom");
            exit(EXIT_FAILURE);
            //On vérifie qu'on a bien récupéré le nom
        }
        printf("pat ok\n");
        applis[i].path = pat;
        printf("i = %d : path = %s\n",i,applis[i].path);
        
        lecture = fgets(line, LINESIZE, fichier);
        if (lecture == NULL){
            perror("nous n'avons pas récupéré la chaine");
            exit(EXIT_FAILURE);
        //On vérifie que fgets a bien fonctionné
        }
        
        nbrArg = parseNumber(line);
        if (nbrArg == NULL)
        {
            perror("nous n'avons pas pu récupérer le nombre d'arguments, soit il est nul soit on le considère nul");
        //On vérifie qu'on a bien récupéré le nombre d'arguments
        }
        printf("nbrArg ok %d\n", nbrArg);
        applis[i].nbArg = nbrArg;
        printf("i = %d : nbArg = %d",i,applis[i].nbArg);
        
        if(nbrArg != 0){
            printf("je suis dans le if/n");
            applis[i].args = malloc((applis[i].nbArg + 1)*sizeof(char*));
            printf("je suis dans le if/n");            
            if(applis[i].args == NULL){
                perror("impossible d'allouer de la mémoire au tableau d'arguments");
                exit(EXIT_FAILURE);
            }
            lecture = fgets(line, LINESIZE, fichier);
            if (lecture == NULL){
                perror("nous n'avons pas récupéré la chaine");
                exit(EXIT_FAILURE);
            }            
            for (int j = 0; j < applis[i].nbArg; j++){
                lecture = fgets(line, LINESIZE, fichier);
                if (lecture == NULL){
                    perror("nous n'avons pas récupéré la chaine");
                    exit(EXIT_FAILURE);
                } 
                arg = malloc(strlen(line) * sizeof(char));
                strcpy(arg, line);
                removeCRLF(arg);
                applis[i].args[j] = arg;
                printf("i = %d : j = %d : arg = %s\n",i,j,applis[i].args[j]);
            }
            applis[i].args[applis[i].nbArg] = NULL;
        }
        else{
            applis[i].args = malloc(sizeof(char*));
            if(applis[i].args == NULL){
                perror("impossible d'allouer de la mémoire au tableau d'arguments");
                exit(EXIT_FAILURE);
            }
            applis[i].args[applis[i].nbArg] = NULL;
            printf("pas d'argument \n");
        }
               
    }
    
    fclose(fichier);
    
    while (nbProcessus < nbr_app){
        pid = fork();
        applis[nbProcessus].id = pid;
        if (pid == -1){
            perror("erreur fork");
            exit(EXIT_FAILURE);
            //Message d'erreur si le fork n'a pas marché
        }
        if (pid == 0){
            if (nbProcessus != 0){
                printf("Je suis dans le fils. Mon pid est : %d et celui de mon père est % d. Voici le path que j'exécute : %s\n", getpid(), getppid(),applis[nbProcessus].path);
            execv (applis[nbProcessus].path, applis[nbProcessus].args);
            //On utilise execv pour lancer l'application
            sleep(1);
            }
            nbProcessus++;
        }
        else if(nbProcessus == 0){
            printf("Je suis dans le père. Mon pid est : %d et celui de mon fils est %d. Voici le path que j'exécute : %s\n", getpid(), pid, applis[nbProcessus].path);
            execv(applis[nbProcessus].path, applis[nbProcessus].args);
            struct sigaction S;
            /* Nous définissons notre structure pour traiter le signal */
            S.sa_handler = signal_handler;
        }
    }
    while(1) {
        pid = wait(NULL);
        if(pid != -1) {
            for(int i = 0; i < nbr_app; i++){
                if(pid == applis[i].id)
                    printf("%s s'est terminée\n", applis[i].name);
            }
        }
    }

    for(int i = 0; i<nbr_app; i++){
        free(applis[i].args);
    }
    free(applis);
    
    return 0;
    
}
